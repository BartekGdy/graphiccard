package graphicCard;

/**
 * Created by RENT on 2017-09-18.
 */
public interface GraphicsSetting {

    void getNeededProcessingPower();

    void processFrame(int[][] frame);


}
