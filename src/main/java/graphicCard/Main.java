package graphicCard;

import java.util.Scanner;

/**
 * Created by RENT on 2017-09-18.
 */
public class Main {

    public static void main(String[] args) {
        GraphicCard graphicCard = new GraphicCard();
        Scanner scanner = new Scanner(System.in);
        String command;
        while (true) {
            System.out.println("Wybierz storowni: HD/Mid/Low");
            command = scanner.nextLine().trim().toLowerCase();
            if (command.equals("q")) {
                break;
            }
            switch (command) {
                case "hd":
                    graphicCard.setGraphicsSetting(new HDSetting());
                    break;
                case "mid":
                    graphicCard.setGraphicsSetting(new MediumSetting());
                    break;
                case "low":
                    graphicCard.setGraphicsSetting(new LowSetting());
                    break;
            }
            graphicCard.getGraphicsSetting().getNeededProcessingPower();
        }
    }
}
