package graphicCard;

/**
 * Created by RENT on 2017-09-18.
 */
public class GraphicCard {
    private Resolution resolution;
    private Colors color;
    private Format format;
    private GraphicsSetting graphicsSetting;

    public GraphicCard() {

    }

    public Resolution getResolution() {
        return resolution;
    }

    public GraphicsSetting getGraphicsSetting() {
        return graphicsSetting;
    }

    public void setGraphicsSetting(GraphicsSetting graphicsSetting) {
        this.graphicsSetting = graphicsSetting;
    }

    public void setResolution(Resolution resolution) {
        this.resolution = resolution;
    }

    public Colors getColor() {
        return color;
    }

    public void setColor(Colors color) {
        this.color = color;
    }

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }
}
