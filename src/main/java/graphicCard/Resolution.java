package graphicCard;

/**
 * Created by RENT on 2017-09-18.
 */
public enum Resolution {
    LOW("800:600"),MID("1024:720"),HIGH("1600:800");

    private String string;

    Resolution(String string) {
        this.string = string;
    }

    public String getString() {
        return string;
    }
}
